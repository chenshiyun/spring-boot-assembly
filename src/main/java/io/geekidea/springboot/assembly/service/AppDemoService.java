package io.geekidea.springboot.assembly.service;

import com.baomidou.mybatisplus.extension.service.IService;
import io.geekidea.springboot.assembly.entity.AppDemo;

import java.util.List;


public interface AppDemoService extends IService<AppDemo> {

    List<AppDemo> getxx();

}
