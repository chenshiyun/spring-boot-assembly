package io.geekidea.springboot.assembly.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import io.geekidea.springboot.assembly.entity.AppDemo;
import io.geekidea.springboot.assembly.mapper.AppDemoMapper;
import io.geekidea.springboot.assembly.service.AppDemoService;
import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class AppDemoServiceImpl extends ServiceImpl<AppDemoMapper, AppDemo> implements AppDemoService {

    @Autowired
    private AppDemoMapper appDemoMapper;


    @Override
    public List<AppDemo> getxx() {
        return appDemoMapper.getxx();
    }
}
