package io.geekidea.springboot.assembly.web;


import io.geekidea.springboot.assembly.service.AppDemoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/demo")
public class AppUserController {


    @Autowired
    private AppDemoService appDemoService;

    @GetMapping(value = "/list")
    public ResponseEntity<Object> list() {
        return ResponseEntity.ok(appDemoService.getxx());
    }


    @GetMapping(value = "/list2")
    public ResponseEntity<Object> list2() {
        return ResponseEntity.ok(appDemoService.list());
    }



}
