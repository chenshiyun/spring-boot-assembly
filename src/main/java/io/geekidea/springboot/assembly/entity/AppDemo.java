package io.geekidea.springboot.assembly.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * @author Eastern unbeaten
 * @email chenshiyun2011@163.com
 * @data 2019-06-22
 */
@Data
@TableName("APP_DEMO")
public class AppDemo {

    private String id;
    private String name;
    private LocalDateTime createTime;
}
