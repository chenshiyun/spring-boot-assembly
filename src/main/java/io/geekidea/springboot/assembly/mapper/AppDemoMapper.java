package io.geekidea.springboot.assembly.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import io.geekidea.springboot.assembly.entity.AppDemo;

import java.util.List;


public interface AppDemoMapper extends BaseMapper<AppDemo> {

    List<AppDemo> getxx();
}
