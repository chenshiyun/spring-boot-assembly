

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for APP_DEMO
-- ----------------------------
DROP TABLE IF EXISTS `APP_DEMO`;
CREATE TABLE `APP_DEMO` (
  `ID` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `NAME` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `CREATE_TIME` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of APP_DEMO
-- ----------------------------
BEGIN;
INSERT INTO `APP_DEMO` VALUES ('ADSSADASDASDA', '张三', '2019-06-22 22:00:57');
INSERT INTO `APP_DEMO` VALUES ('asdasdad', '阿斯达大', '2019-06-22 22:01:08');
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
